let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur",],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},

	talk: function(){
		console.log("Pikachu! I choose you!")
	}
}
console.log(trainer)

// Access properties via dot notation
console.log("Result of dot notation:");
console.log(trainer.name);

// Access properties via square brackets
console.log("Result of square brackets:");
console.log(trainer['pokemon']);

// Invoke the talk method
console.log("Result of talk method:")
trainer.talk();

function Pokemon(name, level){
	//properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//methods
	this.tackle = function(target){
		let targetHealth = target.health - this.attack

		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to " + Number(targetHealth));

		if(targetHealth <= 0){
			target.faint()
		}
	}

	this.faint = function(){
		console.log(this.name + " fainted.")
	}
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu)
let geodude = new Pokemon("Geodude", 8);
console.log(geodude)
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo)

geodude.tackle(pikachu);
mewtwo.tackle(geodude);



